const reverse = require("./index");

it("should reverse an array", () => {
  expect(reverse(["Apple", "Banana", "Orange", "Coconut"])).toEqual([
    "Coconut",
    "Orange",
    "Banana",
    "Apple",
  ]);
});
