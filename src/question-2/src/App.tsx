import React, { useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Box, Grid, Pagination, Paper } from "@mui/material";
import styled from "@emotion/styled";
import { CatFact, CatFactResponse } from "./types";
import getCatFacts from "./api";
import { AxiosResponse } from "axios";

import EmojiObjectsOutlinedIcon from "@mui/icons-material/EmojiObjectsOutlined";
import { compare, getRandomColor } from "./utils";

const App = () => {
  const [catFacts, setCatFacts] = useState<CatFact[]>([]);
  const [catFactPagination, setCatFactPagination] = useState<CatFactResponse>();

  const onPageChange = async (
    event: React.ChangeEvent<unknown>,
    page: number
  ) => {
    const response: AxiosResponse<CatFactResponse> = await getCatFacts(page);
    setCatFacts(response?.data?.data.sort(compare));
    setCatFactPagination(response?.data);
  };

  const fetchCatFacts = async () => {
    const response: AxiosResponse<CatFactResponse> = await getCatFacts();
    setCatFacts(response?.data?.data.sort(compare));
    setCatFactPagination(response?.data);
  };

  useEffect(() => {
    fetchCatFacts();
  }, []);
  return (
    <Box sx={{ flexGrow: 1 }} padding={2}>
      <Grid container spacing={2}>
        <Grid item xs={12} textAlign={"center"}>
          <h2>Welcome to cat facts</h2>
        </Grid>
        {catFacts?.map((catFact) => (
          <Grid item lg={4} md={12} xs={12}>
            <Paper
              variant="outlined"
              style={{
                textAlign: "center",
                minHeight: 200,
                maxHeight: 250,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <EmojiObjectsOutlinedIcon
                fontSize="large"
                style={{ color: getRandomColor() }}
              />
              <h3>{catFact.fact}</h3>
            </Paper>
          </Grid>
        ))}

        <Grid
          item
          xs={12}
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Pagination
            count={catFactPagination?.last_page}
            variant="outlined"
            shape="rounded"
            onChange={onPageChange}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default App;
