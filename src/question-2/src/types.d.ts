export interface CatFactResponse {
  current_page: number;
  data: CatFact[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Array;
  next_page_url: string;
  path: string;
  per_page: string;
  prev_page_url: string;
  to: number;
  total: number;
}

export interface CatFact {
  fact: string;
  length: number;
}
