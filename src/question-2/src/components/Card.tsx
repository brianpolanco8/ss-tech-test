import styled from "@emotion/styled";
import { Paper } from "@mui/material";
import React from "react";

type Props = {};

const Item = styled(Paper)(({ theme }) => ({
  textAlign: "center",
}));

const Card = (props: Props) => {
  return <Item />;
};

export default Card;
