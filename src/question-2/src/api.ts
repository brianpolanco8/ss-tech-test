import axios from "axios";
import { CatFact, CatFactResponse } from "./types";

const getCatFacts = async (page: number = 1) => {
  return axios.get(`https://catfact.ninja/facts?limit=9&page=${page}`);
};

export default getCatFacts;
