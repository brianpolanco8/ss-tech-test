import { CatFact } from "../types";

export const getRandomColor = () => {
  var letters = "0123456789ABCDEF";
  var color = "#";
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export const compare = (a: CatFact, b: CatFact) => {
  if (a.fact < b.fact) {
    return -1;
  }

  if (a.fact > b.fact) {
    return 1;
  }

  return 0;
};
